Base docker image for Arch Linux builds of ALA. The purpose here is to reduce redundant downloads that occur with each build.

The package list can be found [here](https://kayg.org/gitea/ALA/archlinux-docker/src/branch/master/packages).

Not available on Docker Hub yet.

